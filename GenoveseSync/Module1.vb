﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Runtime.Serialization
Imports System.Web
Imports Newtonsoft.Json

Module Module1

   Friend Const CONNECTION_STRING As String = "Data Source=localhost\;Initial Catalog=Genovese;Persist Security Info=True;User ID=sa;Password=Emm3.75!"

   Friend Const REST_API_HOST As String = "http://89.107.94.205:8080/datasnap/rest/TDSArticoliServerModule/"
   Friend Const REST_API_USERNAME = "magis"
   Friend Const REST_API_PASSWORD = "magis"

   Friend Const SMTP_HOST As String = "smtp.emmemedia.com"
   Friend Const SMTP_SENDER As String = "clienti@emmemedia.com"
   Friend Const SMTP_RECIPIENT As String = "claudio.trenca@emmemedia.com"
   Friend Const SMTP_USERNAME As String = "clienti@emmemedia.com"
   Friend Const SMTP_PASSWORD As String = "727clienti"

   Friend Const UNAVAILABLE_IMAGE As String = "immagine-non-disponibile.jpg"

   Friend Property AppPath As String = My.Application.Info.DirectoryPath.ToString
   Friend Property ImagePath As String = "C:\HostingSpaces\genovesepelletterie\www.genovesepelletterie.it\wwwroot\Public\Prodotti\"
   Friend Property ExecutionStatus As Boolean = True
   Friend Property LogFileName As String = Module1.AppPath & "\log_" & Replace(Date.Now.Date, "/", "-") & ".txt"

   Dim RestApiName As String = String.Empty
   Dim JsonResponse As String = String.Empty
   Dim TruncateTable As Boolean = False

   Sub Main()

      WriteLog("START IMPORT")

      HttpRestGet("GetArticoli01")
      HttpRestGet("GetCategorieArticoli")
      HttpRestGet("GetListaColori")
      HttpRestGet("GetListaModelli")
      HttpRestGet("GetListaCampionari")
      HttpRestGet("GetListaPacchetti")
      HttpRestGet("GetListaGruppiMerceologici")
      HttpRestGet("GetGiacenzeTotali")

      GetListaImmaginiColore()
      GetListaImmaginiArticolo()

      WriteLog("END IMPORT")

      SendMail("GENOVESE - SINCRONIZZAZIONE DEL " & Date.Now.ToShortDateString & " - " & CStr(ExecutionStatus), "", LogFileName)

   End Sub

   Private Sub HttpRestGet(ByVal ApiMethod As String, Optional ByVal CodiceArticolo As String = "")
      Try
         ' Dim param As String = IIf(String.IsNullOrEmpty(CodiceArticolo), String.Empty, ("(""" & CodiceArticolo & """ )"))      *** modifica del 31/03/2017

         Dim param As String = IIf(String.IsNullOrEmpty(CodiceArticolo), String.Empty, "/" & CodiceArticolo)
         Dim request As WebRequest = WebRequest.Create(REST_API_HOST & ApiMethod & param)
         Dim HostCredential As New NetworkCredential(REST_API_USERNAME, REST_API_PASSWORD)
         request.Credentials = HostCredential

         Dim response As WebResponse = request.GetResponse()
         If response IsNot Nothing Then
            WriteLog(ApiMethod & param & ":" & DirectCast(response, HttpWebResponse).StatusDescription)

            Dim dataStream As Stream = response.GetResponseStream()
            Dim reader As New StreamReader(dataStream)
            Dim responseFromServer As String = reader.ReadToEnd()
            reader.Close()
            response.Close()

            Dim result As JsonContract.ResultWrapper = JsonConvert.DeserializeObject(Of JsonContract.ResultWrapper)(responseFromServer)
            Dim resultSet As JsonContract.ResultSet = result.result(0)
            Dim dtResult As New DataTable

            Select Case ApiMethod
               Case "GetArticoli01"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeArticoli(resultSet)
                  WriteDataTable(dtResult, ApiMethod)
                  GetListaArticoli()
               Case "GetCategorieArticoli"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeCategorie(resultSet)
                  WriteDataTable(dtResult, ApiMethod)
               Case "GetListaColori"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeListaColori(resultSet)
                  WriteDataTable(dtResult, ApiMethod)
               Case "GetListaModelli"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeListaModelli(resultSet)
                  WriteDataTable(dtResult, ApiMethod)
               Case "GetTaglieArticolo"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeTaglieArticolo(resultSet, CodiceArticolo)
                  WriteDataTable(dtResult, ApiMethod, False)
               Case "GetColoriArticolo01"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeColoreArticolo(resultSet, CodiceArticolo)
                  WriteDataTable(dtResult, ApiMethod, False)
               Case "GetGiacenzeTotali"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeGiacenze(resultSet)
                  WriteDataTable(dtResult, ApiMethod)
               Case "GetListaCampionari"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeListaCampionari(resultSet)
                  WriteDataTable(dtResult, ApiMethod)
               Case "GetListaPacchetti"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeListaPacchetti(resultSet)
                  WriteDataTable(dtResult, ApiMethod)
               Case "GetListaGruppiMerceologici"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeListaGruppiMerceologici(resultSet)
                  WriteDataTable(dtResult, ApiMethod)
               Case "GetListaImmaginiArticolo"
                  dtResult.Clear()
                  dtResult = Deserialize.DeserializeListaImmagini(resultSet, CodiceArticolo)
                  WriteDataTable(dtResult, ApiMethod, False)
            End Select
         Else
            WriteLog("NO RESPONSE FROM " & ApiMethod)
         End If
      Catch ex As Exception
         WriteLog(ex.Message)
         ExecutionStatus = False
      End Try

   End Sub

   Private Sub WriteDataTable(ByRef dt As DataTable, ByVal tableName As String, Optional ByVal deleteTable As Boolean = True)

      Dim conteggiocolonne As Integer = dt.Columns.Count
      Dim conteggiorighe As Integer = dt.Rows.Count

      Using conn As New SqlConnection(CONNECTION_STRING)
         conn.Open()

         If deleteTable = True Then
            Using cmd As New SqlCommand("DELETE FROM " & tableName, conn)
               cmd.ExecuteNonQuery()
            End Using
         Else
            If TruncateTable = False Then
               Using cmd As New SqlCommand("DELETE FROM " & tableName, conn)
                  cmd.ExecuteNonQuery()
               End Using
               TruncateTable = True
            End If
         End If

         Using copy As New SqlBulkCopy(conn, SqlBulkCopyOptions.TableLock, Nothing)
            copy.DestinationTableName = tableName
            copy.BatchSize = 50
            copy.NotifyAfter = 50
            For i = 0 To dt.Columns.Count - 1
               copy.ColumnMappings.Add(i, i)
            Next
            Try
               If deleteTable = True Then WriteLog("BULKINSERT - Start copy " & tableName)
               copy.WriteToServer(dt)
               If deleteTable = True Then WriteLog("BULKINSERT - End copy " & tableName)
            Catch ex As Exception
               Throw ex
               WriteLog(ex.Message)
               Dim dataExceptionFound As Boolean = False
               Dim tmpException As Exception = ex
               While tmpException IsNot Nothing
                  If TypeOf tmpException Is SqlException AndAlso tmpException.Message.Contains("constraint") Then
                     dataExceptionFound = True
                     Exit While
                  End If
                  tmpException = tmpException.InnerException
               End While
               If dataExceptionFound Then
                  Dim errorMessage As String = conn.ConnectionString & " " & copy.DestinationTableName
                  Throw New Exception(errorMessage, ex)
               End If
            Finally
               conn.Close()
            End Try
         End Using
      End Using

   End Sub

   Private Sub GetListaArticoli()
      Try
         Using conn As New SqlConnection(CONNECTION_STRING)
            conn.Open()
            Using cmd As New SqlCommand("SELECT codice FROM GetArticoli01", conn)
               Dim dt As New DataTable
               dt.Load(cmd.ExecuteReader(CommandBehavior.CloseConnection))

               If dt.Rows.Count > 0 Then
                  'For Each dtr As DataRow In dt.Rows
                  '   HttpRestGet("GetTaglieArticolo", dtr("codice"))
                  'Next
                  TruncateTable = False
                  For Each dtr As DataRow In dt.Rows
                     HttpRestGet("GetColoriArticolo01", dtr("codice"))
                  Next
                  TruncateTable = False
                  For Each dtr As DataRow In dt.Rows
                     HttpRestGet("GetListaImmaginiArticolo", dtr("codice"))
                  Next
               End If

            End Using
         End Using
      Catch ex As Exception
         WriteLog(ex.Message)
         ExecutionStatus = False
      End Try

   End Sub

   Private Sub GetListaImmaginiColore()

      Try
         Using conn As New SqlConnection(CONNECTION_STRING)
            conn.Open()
            Using cmd As New SqlCommand("SELECT NomeImmagine FROM GetColoriArticolo01", conn)
               Dim dtr As SqlDataReader
               dtr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
               If dtr.HasRows Then
                  While dtr.Read
                     Base64ToImage(dtr("NomeImmagine"))
                     If CheckImage(dtr("NomeImmagine")) = False Then
                        UpdateImageName(dtr("NomeImmagine"))
                     End If
                  End While
               End If
               dtr.Close()
            End Using
         End Using
      Catch ex As Exception
         WriteLog(ex.Message)
         ExecutionStatus = False
      End Try

   End Sub

   Private Sub GetListaImmaginiArticolo()
      Try
         Using conn As New SqlConnection(CONNECTION_STRING)
            conn.Open()
            Using cmd As New SqlCommand("SELECT foto FROM GetListaImmaginiArticolo", conn)
               Dim dtr As SqlDataReader
               dtr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
               If dtr.HasRows Then
                  While dtr.Read
                     Base64ToImage(dtr("foto"))
                     '  GetStreamImmagine(dtr("NomeImmagine"))
                  End While
               End If
               dtr.Close()
            End Using
         End Using

      Catch ex As Exception
         WriteLog(ex.Message)
         ExecutionStatus = False
      End Try

   End Sub

   Private Function GetStreamImmagine(ByVal NomeArticolo As String) As Boolean

      Dim request As WebRequest
      Dim response As WebResponse
      Try
         If NomeArticolo.Length > 0 Then

            ' *** MODIFICHE DEL 31/03/2017
            'If NomeArticolo.Contains("\") Then
            '   NomeArticolo = NomeArticolo.Substring(NomeArticolo.LastIndexOf("\") + 1)
            'End If
            'Dim param As String = IIf(String.IsNullOrEmpty(NomeArticolo), String.Empty, ("(""" & NomeArticolo & """)"))

            Dim param As String = HttpUtility.UrlPathEncode(NomeArticolo)

            request = WebRequest.Create(REST_API_HOST & "getStreamImmagine" & param)
            Dim HostCredential As New NetworkCredential(REST_API_USERNAME, REST_API_PASSWORD)
            request.Credentials = HostCredential
            response = request.GetResponse()
            If response IsNot Nothing Then
               WriteLog(request.RequestUri.ToString & " : " & DirectCast(response, HttpWebResponse).StatusDescription)
               Dim dataStream As Stream = response.GetResponseStream()
               Dim img As System.Drawing.Image = System.Drawing.Image.FromStream(dataStream)
               response.Close()
               Try
                  NomeArticolo = NomeArticolo.Replace(" ", "-")
                  img.Save(ImagePath + NomeArticolo, ImageFormat.Jpeg)
                  WriteLog("SAVING FILE " & NomeArticolo)
               Catch ex As Exception
                  WriteLog("ERROR SAVING FILE " & NomeArticolo & " - " & ex.Message)
               End Try
            Else
               WriteLog("NO RESPONSE " & NomeArticolo)
            End If
         Else
            Return False
         End If
      Catch ex As Exception
         If response IsNot Nothing Then response.Close()
         WriteLog(ex.Message)
      End Try

   End Function

   Private Function Base64ToImage(ByVal OriginalFileName As String) As Boolean

      Dim rv As Boolean = False
      Dim request As WebRequest
      Dim response As WebResponse
      Dim ImageFileName As String = OriginalFileName

      Try
         If ImageFileName.Length > 0 Then

            If ImageFileName.Contains("\") Then
               ImageFileName = ImageFileName.Substring(ImageFileName.LastIndexOf("\") + 1)
            End If

            ' *** MODIFICHE DEL 31/03/2017
            ' Dim param As String = IIf(String.IsNullOrEmpty(ImageFileName), String.Empty, ("(""" & ImageFileName & """)"))

            Dim param As String = "/" & (HttpUtility.UrlPathEncode(OriginalFileName)).Replace("\", "%5C")
            request = WebRequest.Create(REST_API_HOST & "GetImmagineBase64" & param)

            Dim HostCredential As New NetworkCredential(REST_API_USERNAME, REST_API_PASSWORD)
            request.Credentials = HostCredential
            response = request.GetResponse()
            WriteLog(request.RequestUri.ToString & " : " & DirectCast(response, HttpWebResponse).StatusDescription)

            Try
               If response IsNot Nothing Then

                  Dim dataStream As Stream = response.GetResponseStream()
                  Dim reader As New StreamReader(dataStream)
                  Dim responseFromServer As String = reader.ReadToEnd()
                  reader.Close()
                  response.Close()

                  Dim result As Newtonsoft.Json.Linq.JObject = JsonConvert.DeserializeObject(responseFromServer)
                  If result IsNot Nothing Then

                     Dim jObject As IQueryable = result.AsQueryable
                     Dim jProperty As Newtonsoft.Json.Linq.JProperty = jObject(0)
                     Dim jArray As Newtonsoft.Json.Linq.JArray = jProperty.Value

                     If Not String.IsNullOrEmpty(jArray(0)) Then

                        Dim b64string As String = jArray(0).ToString.Replace(" ", "+")
                        Dim byteImage() As Byte = Convert.FromBase64String(b64string)
                        Dim ms As New System.IO.MemoryStream(byteImage)
                        Dim img As System.Drawing.Image = System.Drawing.Image.FromStream(ms)

                        Try
                           ImageFileName = ImageFileName.Replace(" ", "-")
                           img.Save(ImagePath + ImageFileName, ImageFormat.Jpeg)
                           rv = True
                           WriteLog("SAVING FILE " & ImageFileName)
                        Catch ex As Exception
                           WriteLog("ERROR SAVING FILE " & ImageFileName & " - " & ex.Message)
                           UpdateImageName(OriginalFileName)
                        End Try

                     End If
                  End If
               Else
                  Return False
               End If
            Catch ex As Exception
               WriteLog(request.RequestUri.ToString & " : " & ex.Message)
               UpdateImageName(OriginalFileName)
               ExecutionStatus = False
               Throw ex
            End Try
         End If

      Catch ex As Exception
         WriteLog(ImageFileName & " : GENERIC ERROR" & " - " & ex.Message)
         UpdateImageName(OriginalFileName)
         ExecutionStatus = False
      Finally
         request = Nothing
         response = Nothing
      End Try

      Return rv

   End Function

   Private Function UpdateImageName(ByVal OriginalFileName As String) As Boolean
      Try
         Using conn As New SqlConnection(CONNECTION_STRING)
            conn.Open()
            Using cmd As New SqlCommand("UPDATE GetColoriArticolo01 SET NomeImmagine = @FileName where NomeImmagine = @OriginalFileName", conn)
               cmd.Parameters.AddWithValue("@FileName", UNAVAILABLE_IMAGE)
               cmd.Parameters.AddWithValue("@OriginalFileName", OriginalFileName)
               cmd.ExecuteNonQuery()
            End Using
            conn.Close()
         End Using
      Catch ex As Exception
         WriteLog(OriginalFileName & " : GENERIC ERROR" & " - " & ex.Message)
      End Try

   End Function

   Private Function CheckImage(ByVal FileName As String) As Boolean

      If FileName.Contains("\") Then
         FileName = FileName.Substring(FileName.LastIndexOf("\") + 1)
      End If

      FileName = FileName.Replace(" ", "-")

      If File.Exists(ImagePath & FileName) Then
         Return True
      Else
         Return False
      End If

   End Function

End Module