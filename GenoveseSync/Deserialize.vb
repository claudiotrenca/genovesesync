﻿Friend Module Deserialize
   Friend Function DeserializeArticoli(ByRef resultSet As JsonContract.ResultSet) As DataTable

      Dim rTable As New DataTable
      Dim iCodice As Integer = -1, iDescrizione As Integer = -1, iCodCategoria As Integer = -1, iComposizione As Integer = -1, iPrezzo As Integer = -1, iPrezzoPromo As Integer = -1, iScontoPromo As Integer = -1, iPromoDaData As Integer = -1, iPromoAData As Integer = -1, iCodModello As Integer = -1, iDescrizioneBreveWeb As Integer = -1, iCodPacchetto As Integer = -1, iCodGruppoMerceologico As Integer = -1, iCodCampionario As Integer = -1, iInVetrina As Integer = -1, iInArrivo As Integer = -1

      Try

         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "CODICE"
                  iCodice = i
               Case "DESCRIZIONE"
                  iDescrizione = i
               Case "CODCATEGORIA"
                  iCodCategoria = i
               Case "COMPOSIZIONE"
                  iComposizione = i
               Case "PREZZO"
                  iPrezzo = i
               Case "PREZZOPROMO"
                  iPrezzoPromo = i
               Case "SCONTOPROMO"
                  iScontoPromo = i
               Case "PROMODADATA"
                  iPromoDaData = i
               Case "PROMOADATA"
                  iPromoAData = i
               Case "CODMODELLO"
                  iCodModello = i
               Case "DESCRIZIONEBREVEWEB"
                  iDescrizioneBreveWeb = i
               Case "CODPACCHETTO"
                  iCodPacchetto = i
               Case "CODGRUPPOMERCEOLOGICO"
                  iCodGruppoMerceologico = i
               Case "CODCAMPIONARIO"
                  iCodCampionario = i
               Case "INVETRINA"
                  iInVetrina = i
               Case "INARRIVO"
                  iInArrivo = i
            End Select
         Next

         Dim NumDec As Decimal = 0.00D

         Dim articoli As New List(Of JsonContract.Articoli)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.Articoli()
            If iCodice <> -1 Then item.Codice = DirectCast(record(iCodice), String)
            If iDescrizione <> -1 Then item.Descrizione = DirectCast(record(iDescrizione), String)
            If iCodCategoria <> -1 Then item.CodCategoria = DirectCast(record(iCodCategoria), String)
            If iComposizione <> -1 Then item.Composizione = DirectCast(record(iComposizione), String)
            If iPrezzo <> -1 Then item.Prezzo = IIf(String.IsNullOrEmpty(record(iPrezzo)), 0.00D, record(iPrezzo))
            If iPrezzoPromo <> -1 Then item.PrezzoPromo = IIf(String.IsNullOrEmpty(record(iPrezzoPromo)), 0.00D, record(iPrezzoPromo))  'DirectCast(record(iPrezzoPromo), Decimal)
            If iScontoPromo <> -1 Then item.ScontoPromo = IIf(String.IsNullOrEmpty(record(iScontoPromo)), 0.00D, record(iScontoPromo))  'DirectCast(record(iScontoPromo), Decimal)
            If iPromoDaData <> -1 Then item.PromoDaDAta = DirectCast(record(iPromoDaData), String)
            If iPromoAData <> -1 Then item.PromoAData = DirectCast(record(iPromoAData), String)
            If iCodModello <> -1 Then item.CodModello = DirectCast(record(iCodModello), String)
            If iDescrizioneBreveWeb <> -1 Then item.DescrizioneBreveWeb = DirectCast(record(iDescrizioneBreveWeb), String)
            If iCodPacchetto <> -1 Then item.CodPacchetto = DirectCast(record(iCodPacchetto), String)
            If iCodGruppoMerceologico <> -1 Then item.CodGruppoMerceologico = DirectCast(record(iCodGruppoMerceologico), String)
            If iCodCampionario <> -1 Then item.CodCampionario = DirectCast(record(iCodCampionario), String)
            If iInVetrina <> -1 Then item.InVetrina = CInt(record(iInVetrina))
            If iInArrivo <> -1 Then item.InArrivo = CInt(record(iInArrivo))
            articoli.Add(item)
         Next

         rTable = articoli.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False

      End Try

      Return rTable

   End Function

   Friend Function DeserializeCategorie(ByRef resultSet As JsonContract.ResultSet) As DataTable

      Dim rTable As New DataTable
      Dim iID As Integer = -1, iCodice As Integer = -1, iNome As Integer = -1, iPadre As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "ID"
                  iID = i
               Case "CODICE"
                  iCodice = i
               Case "NOME"
                  iNome = i
               Case "PADRE"
                  iPadre = i

            End Select
         Next

         Dim CategorieArticoli As New List(Of JsonContract.CategorieArticoli)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.CategorieArticoli()
            If iID <> -1 Then item.Id = IIf(String.IsNullOrEmpty(record(iID)), Nothing, record(iID))     ' CInt(record(iID))
            If iCodice <> -1 Then item.Codice = DirectCast(record(iCodice), String)
            If iNome <> -1 Then item.Nome = DirectCast(record(iNome), String)
            If iPadre <> -1 Then item.Padre = IIf(String.IsNullOrEmpty(record(iPadre)), Nothing, record(iPadre))
            CategorieArticoli.Add(item)
         Next

         rTable = CategorieArticoli.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False

      End Try

      Return rTable

   End Function

   Friend Function DeserializeListaColori(ByRef resultSet As JsonContract.ResultSet) As DataTable

      Dim rTable As New DataTable
      Dim iCodice As Integer = -1, iNome As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "CODICE"
                  iCodice = i
               Case "NOME"
                  iNome = i

            End Select
         Next

         Dim ListaColori As New List(Of JsonContract.Colori)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.Colori()
            If iCodice <> -1 Then item.Codice = DirectCast(record(iCodice), String)
            If iNome <> -1 Then item.Nome = DirectCast(record(iNome), String)
            ListaColori.Add(item)
         Next

         rTable = ListaColori.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

   Friend Function DeserializeListaModelli(ByRef resultSet As JsonContract.ResultSet) As DataTable

      Dim rTable As New DataTable
      Dim iCodice As Integer = -1, iNome As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "CODICE"
                  iCodice = i
               Case "NOME"
                  iNome = i

            End Select
         Next

         Dim ListaModelli As New List(Of JsonContract.Modelli)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.Modelli()
            If iCodice <> -1 Then item.Codice = DirectCast(record(iCodice), String)
            If iNome <> -1 Then item.Nome = DirectCast(record(iNome), String)
            ListaModelli.Add(item)
         Next

         rTable = ListaModelli.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

   Friend Function DeserializeListaCampionari(ByRef resultSet As JsonContract.ResultSet) As DataTable

      Dim rTable As New DataTable
      Dim iCodice As Integer = -1, iNome As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "CODICE"
                  iCodice = i
               Case "NOME"
                  iNome = i

            End Select
         Next

         Dim ListaCampionari As New List(Of JsonContract.Campionari)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.Campionari()
            If iCodice <> -1 Then item.Codice = DirectCast(record(iCodice), String)
            If iNome <> -1 Then item.Nome = DirectCast(record(iNome), String)
            ListaCampionari.Add(item)
         Next

         rTable = ListaCampionari.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

   Friend Function DeserializeListaPacchetti(ByRef resultSet As JsonContract.ResultSet) As DataTable

      Dim rTable As New DataTable
      Dim iCodice As Integer = -1, iNome As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "CODICE"
                  iCodice = i
               Case "NOME"
                  iNome = i

            End Select
         Next

         Dim ListaPacchetti As New List(Of JsonContract.Pacchetti)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.Pacchetti()
            If iCodice <> -1 Then item.Codice = DirectCast(record(iCodice), String)
            If iNome <> -1 Then item.Nome = DirectCast(record(iNome), String)
            ListaPacchetti.Add(item)
         Next

         rTable = ListaPacchetti.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

   Friend Function DeserializeListaGruppiMerceologici(ByRef resultSet As JsonContract.ResultSet) As DataTable

      Dim rTable As New DataTable
      Dim iCodice As Integer = -1, iNome As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "CODICE"
                  iCodice = i
               Case "NOME"
                  iNome = i

            End Select
         Next

         Dim ListaGruppiMerceologici As New List(Of JsonContract.GruppiMerceologici)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.GruppiMerceologici()
            If iCodice <> -1 Then item.Codice = DirectCast(record(iCodice), String)
            If iNome <> -1 Then item.Nome = DirectCast(record(iNome), String)
            ListaGruppiMerceologici.Add(item)
         Next

         rTable = ListaGruppiMerceologici.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

   Friend Function DeserializeGiacenze(ByRef resultSet As JsonContract.ResultSet) As DataTable

      Dim rTable As New DataTable
      Dim iCodArticolo As Integer = -1, iCodColore As Integer = -1, iTaglia As Integer = -1, iGiacenza As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "CODARTICOLO"
                  iCodArticolo = i
               Case "CODCOLORE"
                  iCodColore = i
               Case "TAGLIA"
                  iTaglia = i
               Case "GIACENZA"
                  iGiacenza = i

            End Select
         Next

         Dim GiacenzeTotali As New List(Of JsonContract.GiacenzeTotali)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.GiacenzeTotali()
            If iCodArticolo <> -1 Then item.CodArticolo = IIf(String.IsNullOrEmpty(record(iCodArticolo)), Nothing, record(iCodArticolo))
            If iCodColore <> -1 Then item.CodColore = DirectCast(record(iCodColore), String)
            If iTaglia <> -1 Then item.Taglia = DirectCast(record(iTaglia), String)
            If iGiacenza <> -1 Then item.Giacenza = IIf(String.IsNullOrEmpty(record(iGiacenza)), Nothing, record(iGiacenza))
            GiacenzeTotali.Add(item)
         Next

         rTable = GiacenzeTotali.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

   Friend Function DeserializeTaglieArticolo(ByRef resultSet As JsonContract.ResultSet, ByVal codiceArticolo As String) As DataTable

      Dim rTable As New DataTable
      Dim iTaglia As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "TAGLIA"
                  iTaglia = i
            End Select
         Next

         Dim TaglieArticolo As New List(Of JsonContract.TaglieArticolo)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.TaglieArticolo()
            item.CodArticolo = codiceArticolo
            If iTaglia <> -1 Then item.Taglia = DirectCast(record(iTaglia), String)
            TaglieArticolo.Add(item)
         Next

         rTable = TaglieArticolo.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

   Friend Function DeserializeColoreArticolo(ByRef resultSet As JsonContract.ResultSet, ByVal codiceArticolo As String) As DataTable
      ' GetColoriArticolo01  : [["CODICE","ftString",20],["NOME","ftString",50],["CODICEWEB","ftString",10],["NOMEIMMAGINE","ftString",255]]

      Dim rTable As New DataTable
      Dim iCodiceProdotto As Integer = -1, iCodice As Integer = -1, iNome As Integer = -1, iCodiceWeb As Integer = -1, iNomeImmagine As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "CODICE"
                  iCodice = i
               Case "NOME"
                  iNome = i
               Case "CODICEWEB"
                  iCodiceWeb = i
               Case "NOMEIMMAGINE"
                  iNomeImmagine = i
            End Select
         Next

         Dim ColoriArticolo As New List(Of JsonContract.ColoriArticolo)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.ColoriArticolo()
            If iCodice <> -1 Then item.CodiceProdotto = codiceArticolo
            If iCodice <> -1 Then item.Codice = DirectCast(record(iCodice), String)
            If iNome <> -1 Then item.Nome = DirectCast(record(iNome), String)
            If iCodiceWeb <> -1 Then item.CodiceWeb = DirectCast(record(iCodiceWeb), String)
            If iNomeImmagine <> -1 Then item.NomeImmagine = DirectCast(record(iNomeImmagine), String)
            ColoriArticolo.Add(item)
         Next

         rTable = ColoriArticolo.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

   Friend Function DeserializeListaImmagini(ByRef resultSet As JsonContract.ResultSet, ByVal codiceArticolo As String) As DataTable
      ' GetListaImmaginiArticolo(CodArticolo: string): TJSONObject; [["FOTO","ftString",255],["PRINCIPALE","ftSmallint"],["ORDINE","ftSmallint"]]

      Dim rTable As New DataTable
      Dim iCodArticolo As Integer = -1, iFoto As Integer = -1, iPrincipale As Integer = -1, iOrdine As Integer = -1

      Try
         For i As Integer = 0 To resultSet.meta.Count - 1
            Select Case resultSet.meta(i).FieldName
               Case "FOTO"
                  iFoto = i
               Case "PRINCIPALE"
                  iPrincipale = i
               Case "ORDINE"
                  iOrdine = i
            End Select
         Next

         Dim ListaImmaginiArticolo As New List(Of JsonContract.ListaImmaginiArticolo)(resultSet.data.Count)
         For Each record As JsonContract.DataRecord In resultSet.data
            Dim item As New JsonContract.ListaImmaginiArticolo()
            If iFoto <> -1 Then item.CodiceProdotto = codiceArticolo
            If iFoto <> -1 Then item.Foto = System.Web.HttpUtility.UrlDecode(DirectCast(record(iFoto), String)).Replace("/", "\")
            If iPrincipale <> -1 Then item.Principale = IIf(String.IsNullOrEmpty(record(iPrincipale)), Nothing, record(iPrincipale))
            If iOrdine <> -1 Then item.Ordine = IIf(String.IsNullOrEmpty(record(iOrdine)), Nothing, record(iOrdine))
            ListaImmaginiArticolo.Add(item)
         Next

         rTable = ListaImmaginiArticolo.ToDataTable()
      Catch ex As Exception
         WriteLog("EXECUTION ERROR IN : " & System.Reflection.MethodInfo.GetCurrentMethod.Name)
         Module1.ExecutionStatus = False
      End Try

      Return rTable

   End Function

End Module