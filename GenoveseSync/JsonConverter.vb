﻿Imports Newtonsoft.Json

Public Class JsonConverter
   Public Shared Function FromClass(Of T)(data As T, Optional isEmptyToNull As Boolean = False, Optional jsonSettings As JsonSerializerSettings = Nothing) As String
      Dim response As String = String.Empty

      If (data IsNot Nothing) Then
         Try
            response = JsonConvert.SerializeObject(data, jsonSettings)
         Catch ex As Exception
            Debug.WriteLine(ex)
         End Try
      End If
      Return If(isEmptyToNull, (If(response = "{}", "null", response)), response)
   End Function

   Public Shared Function ToClass(Of T)(data As String, Optional jsonSettings As JsonSerializerSettings = Nothing) As T
      Dim response As T = Nothing

      If Not String.IsNullOrEmpty(data) Then
         Try
            response = If(jsonSettings Is Nothing, JsonConvert.DeserializeObject(Of T)(data), JsonConvert.DeserializeObject(Of T)(data, jsonSettings))
         Catch ex As Exception
            Debug.WriteLine(ex)
         End Try
      End If
      Return response
   End Function
End Class