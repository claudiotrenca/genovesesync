﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Runtime.Serialization
Imports System.Web
Imports Newtonsoft.Json

Module Common
   Friend Sub HandleWebException(ByVal wex As WebException)
      Dim msg As String = "Il server non risponde alla richiesta."
      Dim hwr As HttpWebResponse = CType(wex.Response, HttpWebResponse)
      If hwr IsNot Nothing Then
         msg += vbCrLf + String.Format("codice di stato : {0}, messaggio di errore : {1}.", hwr.StatusCode, hwr.StatusDescription)
         Using sreader As System.IO.StreamReader = New System.IO.StreamReader(hwr.GetResponseStream())
            Dim errorMessage As String = sreader.ReadToEnd()
            If Not String.IsNullOrEmpty(errorMessage) Then
               msg += vbCrLf + vbCrLf + "Di seguito i restanti dati inviati dal server :" + vbCrLf + errorMessage
            End If
         End Using
      End If
      WriteLog(msg)
   End Sub

   Friend Sub WriteLog(ByVal Message As String)

      Dim timedate As Date = Date.Now
      Dim msg As String = String.Empty

      Console.WriteLine(Date.Now & vbTab & Message)

      msg = timedate.ToShortDateString & " - " & Format(timedate.Hour, "00") & ":" & Format(timedate.Minute, "00") & ":" & Format(timedate.Second, "00") + vbTab + Message

      If File.Exists(Module1.LogFileName) Then
         Using sw As StreamWriter = File.AppendText(Module1.LogFileName)
            sw.WriteLine(msg)
            sw.Close()
         End Using
      Else
         Dim sw As New System.IO.StreamWriter(Module1.LogFileName, False)
         sw.WriteLine(msg)
         sw.Close()
      End If

   End Sub

   Friend Function SendMail(ByVal subject As String, ByVal body As String, ByVal file As String) As Boolean
      Try
         Using msg As New MailMessage
            msg.From = New MailAddress(SMTP_SENDER)
            msg.To.Add(New MailAddress(SMTP_USERNAME))
            msg.Subject = subject & Date.Now.ToString("dd/MM/yyyy")
            msg.Body = body
            msg.IsBodyHtml = True

            Dim attach As New Attachment(file)
            msg.Attachments.Add(attach)

            Using smtp As New SmtpClient(SMTP_HOST)
               smtp.Port = 25
               smtp.Credentials = New Net.NetworkCredential(SMTP_RECIPIENT, SMTP_PASSWORD)
               smtp.Send(msg)
               msg.Dispose()
            End Using
         End Using
      Catch ex As Exception
         WriteLog("ERROR SENDING MAIL")
      End Try

   End Function

End Module