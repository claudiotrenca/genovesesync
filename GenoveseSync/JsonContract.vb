﻿Public Class JsonContract

   Public Class ResultWrapper
      Public Property result As List(Of ResultSet)
   End Class

   ' { meta: [...], data: [...] }
   Public Class ResultSet
      Public Property meta As List(Of MetaField)
      Public Property data As List(Of DataRecord)
   End Class

   ' ["ColumnName", ...]
   Public Class MetaField : Inherits List(Of Object)
      Public ReadOnly Property FieldName As String
         Get
            Return DirectCast(Me(0), String)
         End Get
      End Property
   End Class

   ' [RowField, ...]
   Public Class DataRecord : Inherits List(Of Object)
   End Class

   ' GetArticoli01  : [["CODICE","ftString",16],["DESCRIZIONE","ftString",150],["COMPOSIZIONE","ftString",100],["CODCATEGORIA","ftString",50],["PREZZO","ftFloat"],["PREZZOPROMO","ftFloat"],["SCONTOPROMO","ftFloat"],["PROMODADATA","ftTimeStamp"],["PROMOADATA","ftTimeStamp"],["CODMODELLO","ftString",16],["DESCRIZIONEBREVEWEB","ftString",100],["CODPACCHETTO","ftString",6],["CODGRUPPOMERCEOLOGICO","ftString",6],["CODCAMPIONARIO","ftString",6],["INVETRINA","ftInteger"],["INARRIVO","ftInteger"]]
   Public Class Articoli
      Public Property Codice As String
      Public Property Descrizione As String
      Public Property Composizione As String
      Public Property CodCategoria As String
      Public Property Prezzo As Decimal
      Public Property PrezzoPromo As Decimal
      Public Property ScontoPromo As Decimal
      Public Property PromoDaDAta As String
      Public Property PromoAData As String
      Public Property CodModello As String
      Public Property DescrizioneBreveWeb As String
      Public Property CodPacchetto As String
      Public Property CodGruppoMerceologico As String
      Public Property CodCampionario As String
      Public Property InVetrina As Integer
      Public Property InArrivo As Integer
   End Class

   Public Class CategorieArticoli
      Public Property Id As Integer
      Public Property Codice As String
      Public Property Nome As String
      Public Property Padre As Integer
   End Class

   Public Class TaglieArticolo
      Public Property CodArticolo As String
      Public Property Taglia As String
   End Class

   ' GetColoriArticolo01  : [["CODICE","ftString",20],["NOME","ftString",50],["CODICEWEB","ftString",10],["NOMEIMMAGINE","ftString",255]]
   Public Class ColoriArticolo
      Public Property CodiceProdotto As String
      Public Property Codice As String
      Public Property Nome As String
      Public Property CodiceWeb As String
      Public Property NomeImmagine As String
   End Class

   'GetListaColori: TJSONObject;
   Public Class Colori
      Public Property Codice As String
      Public Property Nome As String
   End Class

   Public Class GiacenzeTotali
      Public Property CodArticolo As String
      Public Property CodColore As String
      Public Property Taglia As String
      Public Property Giacenza As Integer
   End Class

   ' GetListaModelli : [["CODICE","ftString",6],["NOME","ftString",50]]
   Public Class Modelli
      Public Property Codice As String
      Public Property Nome As String
   End Class

   ' GetListaCampionari : [["CODICE","ftString",6],["NOME","ftString",50]]
   Public Class Campionari
      Public Property Codice As String
      Public Property Nome As String
   End Class

   ' GetListaPacchetti : [["CODICE","ftString",6],["NOME","ftString",50]]
   Public Class Pacchetti
      Public Property Codice As String
      Public Property Nome As String
   End Class

   ' GetListaGruppiMerceologici : [["CODICE","ftString",6],["NOME","ftString",50]]
   Public Class GruppiMerceologici
      Public Property Codice As String
      Public Property Nome As String
   End Class

   ' GetListaImmaginiArticolo(CodArticolo: string): TJSONObject; [["FOTO","ftString",255],["PRINCIPALE","ftSmallint"],["ORDINE","ftSmallint"]]
   Public Class ListaImmaginiArticolo
      Public Property CodiceProdotto As String
      Public Property Foto As String
      Public Property Principale As Integer
      Public Property Ordine As Integer
   End Class

   Public Class ImageWrapper
      Public Property result As List(Of String)
   End Class

   Public Class ImageResultSet
      Public Property data As String
   End Class

End Class